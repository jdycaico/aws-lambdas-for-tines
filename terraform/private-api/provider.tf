#https://reflectoring.io/terraform-aws/
terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.53.0"
    }
  }
}

provider "aws" {
  default_tags {
    tags = { Project = "Tines-Lambdas", Team = "Detection Engineering" }
  }
}