locals {
  private_vpc_id   = "vpc-0b1c4933eb34b1e28"
  subnet_ids       = ["subnet-05a9273bcb6b9f607"] #us-west-2a
  sg_ids           = ["sg-014d37101eb0bbc09"]     #default security group
  region           = "us-west-2"
  account_id       = 800303436368 #aws-personal
  ami_ubuntu22_x86 = "ami-0735c191cf914754d"
  ec2_sshkey_name  = "TestBox2-ed255-pem"
}

#〜〜〜〜〜〜〜〜〜〜〜〜〜〜〜〜Lambdas〜〜〜〜〜〜〜〜〜〜〜〜〜〜〜〜〜〜〜〜〜〜〜
# https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/lambda_function
# TODO: add lambda-apikey-permissions
resource "aws_lambda_function" "phish_imager" {
  image_uri     = "${local.account_id}.dkr.ecr.${local.region}.amazonaws.com/phish-imager:latest"
  function_name = "phish-imager"
  package_type  = "Image"
  role          = "arn:aws:iam::${local.account_id}:role/Lambda-StandardRole"
  architectures = [
    "arm64"
  ]
}

resource "aws_lambda_permission" "phish_imager_permission" {
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.phish_imager.function_name
  principal     = "apigateway.amazonaws.com"
  source_arn    = "arn:aws:execute-api:${local.region}:${local.account_id}:${aws_api_gateway_rest_api.tines_private_api.id}/${aws_api_gateway_stage.tines_api_stage.stage_name}/POST/${aws_lambda_function.phish_imager.function_name}"
}

resource "aws_lambda_function" "pdf_url_extractor" {
  image_uri     = "${local.account_id}.dkr.ecr.${local.region}.amazonaws.com/pdf-url-extractor:latest"
  function_name = "pdf-url-extractor"
  package_type  = "Image"
  role          = "arn:aws:iam::${local.account_id}:role/Lambda-StandardRole"
  architectures = [
    "arm64"
  ]
}
resource "aws_lambda_permission" "pdf_url_extractor_permisson" {
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.pdf_url_extractor.function_name
  principal     = "apigateway.amazonaws.com"
  source_arn    = "arn:aws:execute-api:${local.region}:${local.account_id}:${aws_api_gateway_rest_api.tines_private_api.id}/${aws_api_gateway_stage.tines_api_stage.stage_name}/POST/${aws_lambda_function.pdf_url_extractor.function_name}"
}

# Attempting of addition of Lambda-specific VPC/SG as per ThreatModel best practices -- leaving for now
/*
resource "aws_vpc" "lambdas_vpc" {
  cidr_block = "172.32.0.0/16"
  instance_tenancy = "default"
  tags = {
    Name = "main"
  }
}

resource "aws_security_group" "lambdas_sg" {
  
}

#resource vpcendpoints? or just "associate" the lambdas with the vpc? probably the latter
*/




#〜〜〜〜〜〜〜〜〜〜〜〜〜〜〜〜ApiGateway〜〜〜〜〜〜〜〜〜〜〜〜〜〜〜〜〜〜〜〜〜〜〜
resource "aws_api_gateway_rest_api" "tines_private_api" {
  name = "tines_private_api"
  endpoint_configuration {
    types            = ["PRIVATE"]
    vpc_endpoint_ids = [aws_vpc_endpoint.tines_api_vpcendpoint.id] #!Ref tines_api_vpcendpoint
  }
  policy = jsonencode({ "Version" : "2012-10-17", "Statement" : [{ "Effect" : "Deny", "Principal" : "*", "Action" : "execute-api:Invoke", "Resource" : "execute-api:/v1/POST/*", "Condition" : { "StringNotEquals" : { "aws:sourceVpc" : "${local.private_vpc_id}" } } }, { "Effect" : "Allow", "Principal" : "*", "Action" : "execute-api:Invoke", "Resource" : "execute-api:/v1/POST/*" }] }
  )
  body = jsonencode({
    "info" : {
      "version" : "1.0",
      "title" : "AWS::StackName"
    },
    "paths" : {
      "/phish-imager" : {
        "post" : {
          "x-amazon-apigateway-integration" : {
            "httpMethod" : "POST",
            "type" : "aws_proxy",
            "uri" : "arn:aws:apigateway:${local.region}:lambda:path/2015-03-31/functions/${aws_lambda_function.phish_imager.arn}/invocations"
          },
          "security" : [
            {
              "api_key" : []
            }
          ],
          "responses" : {}
        }
      },
      "/pdf-url-extractor" : {
        "post" : {
          "x-amazon-apigateway-integration" : {
            "httpMethod" : "POST",
            "type" : "aws_proxy",
            "uri" : "arn:aws:apigateway:${local.region}:lambda:path/2015-03-31/functions/${aws_lambda_function.pdf_url_extractor.arn}/invocations"
          },
          "security" : [
            {
              "api_key" : []
            }
          ],
          "responses" : {}
        }
      }
    },
    "swagger" : "2.0",
    "securityDefinitions" : {
      "api_key" : {
        "type" : "apiKey",
        "name" : "x-api-key",
        "in" : "header"
      }
    }
    }
  )
}

resource "aws_api_gateway_stage" "tines_api_stage" {
  deployment_id = aws_api_gateway_deployment.tines_api_deployment.id
  rest_api_id   = aws_api_gateway_rest_api.tines_private_api.id
  stage_name    = "v1"
}

resource "aws_api_gateway_deployment" "tines_api_deployment" {
  rest_api_id = aws_api_gateway_rest_api.tines_private_api.id
  # adding stage_name fails
  #stage_name = "v1"
  #depends_on = [aws_api_gateway_stage.tines_api_stage]

}

resource "aws_api_gateway_method_settings" "tines_api_method_settings" {
  # part of Stage resource in CF template
  rest_api_id = aws_api_gateway_rest_api.tines_private_api.id
  stage_name  = aws_api_gateway_stage.tines_api_stage.stage_name
  method_path = "*/*" #attempted combine of ResourcePath and HttpMethod
  settings {
    metrics_enabled = true
    logging_level   = "INFO"
  }
}

resource "aws_api_gateway_usage_plan" "tines_api_usage_plan" {
  # needs to reference the apikey? 
  name = "tines_api_usage_plan"
  api_stages {
    api_id = aws_api_gateway_rest_api.tines_private_api.id
    stage  = aws_api_gateway_stage.tines_api_stage.stage_name
  }
  quota_settings {
    limit  = 1000
    offset = 0
    period = "DAY"
  }
}

resource "aws_api_gateway_usage_plan_key" "tines_api_usage_plan_key" {
  key_id        = aws_api_gateway_api_key.tines_api_apikey.id
  key_type      = "API_KEY"
  usage_plan_id = aws_api_gateway_usage_plan.tines_api_usage_plan.id
}

resource "aws_api_gateway_api_key" "tines_api_apikey" {
  name  = "tines_api_apikey"
  value = file("/Users/jonathan.dycaico/_auth/aws/tines_api_apikey") #for dev/test
  #StageKeys:
  #  - RestApiId: !Ref tines_private_api
  #    StageName: !Ref tines_api_stage
}

resource "aws_vpc_endpoint" "tines_api_vpcendpoint" {
  # takes a long time to destroy and recreate
  vpc_id              = local.private_vpc_id
  service_name        = "com.amazonaws.us-west-2.execute-api"
  private_dns_enabled = true # if `conflicting DNS domain error`, just wait 5min and redeploy
  vpc_endpoint_type   = "Interface"
  subnet_ids          = local.subnet_ids
  security_group_ids  = local.sg_ids
}

#〜〜〜〜〜〜〜〜〜〜〜〜〜〜〜〜Other〜〜〜〜〜〜〜〜〜〜〜〜〜〜〜〜〜〜〜〜〜〜〜
resource "aws_instance" "tines_sim_testbox" {
  ami                    = local.ami_ubuntu22_x86
  instance_type          = "t2.micro"
  key_name               = local.ec2_sshkey_name
  vpc_security_group_ids = local.sg_ids
  subnet_id              = local.subnet_ids[0]
}

output "phish_invoke_url" {
  value = "https://${aws_api_gateway_rest_api.tines_private_api.id}.execute-api.${local.region}.amazonaws.com/${aws_api_gateway_stage.tines_api_stage.stage_name}/${aws_lambda_function.phish_imager.function_name}"
}

output "pdf_invoke_url" {
  value = "https://${aws_api_gateway_rest_api.tines_private_api.id}.execute-api.${local.region}.amazonaws.com/${aws_api_gateway_stage.tines_api_stage.stage_name}/${aws_lambda_function.pdf_url_extractor.function_name}"
}

output "ec2_user_host" {
  value = "ubuntu@${aws_instance.tines_sim_testbox.public_dns}"
}