# Tines Lambdas
Implements 2x functions needed by our Tines workflows, implemented in AWS Lambda: 
- **phish-imager**
* * Input: the URL of an image file
* * Output: the base64-encoded image

- **pdf-url-extractor**
* * Input: a base64-encoded PDF file
* * Output: a list of URLs contained within


## Notes:
* The above (dash-separated) are the container names, or function names. The folder names locally are underscore-separated so that pytest can find them.
* The Docker images will have to be pushed to ECR prior to deploying the infrastructure components via Terraform templates
* The azure-pipelines.yml does not push the container to AWS ECR or update the Lambda due to limitations from the team that manages AWS.
* The certificates that get combined in `phish_imager/certs/combine_certs` are not included in the repo as they are company-internal



### Final note for posterity: 
After writing the terraform template, we decided to use AWS AssumeRole to invoke the function from the server, rather than an HTTP API Request. As such, the function code has been changed to read in named variables such as `label_url` from `event` as a dictionary. 

If the app were deployed with API using the terraform template, the `handler()` and validation functions would need to be modified to run `json.loads()` against the `body` key of the incoming event, in order to access the data items within, such as `label_url`.