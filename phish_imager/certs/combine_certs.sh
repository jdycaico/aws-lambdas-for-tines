#!/bin/bash
export CERT_SRC_PATH="/Users/jonathan.dycaico/_auth/certs"
cd "$(dirname "$0")" #https://stackoverflow.com/a/3355423
#echo "PWD=$(pwd)"
cp ./python3.10/site-packages/certifi/cacert.pem $PWD
OLD_SHASUM=$(sha256sum cacert.pem)
cp cacert.pem cacert.bak.pem

for certFile in $CERT_SRC_PATH/DocuSign*.pem; do
    echo "adding $(basename $certFile)..."
    cat $certFile >> cacert.pem
done

NEW_SHASUM=$(sha256sum cacert.pem)

if [ "$NEW_SHASUM" = "$OLD_SHASUM" ]; then 
    echo "ERROR: Certificate file was not updated!"
    return 1
fi

