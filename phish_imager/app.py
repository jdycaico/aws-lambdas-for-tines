"""Phish-Imager
AWS Lambda function to process PNG images ("screenshots") from
the Pescatore anti-phishing system.
Applies a label indicating the source of the screenshot.

Process:
> downloads an image from image_link
> applies a text label to the top of the image
> returns the base64-encoded resulting image, along with a uuid

Input: event(dict), containing keys: label_url, image_link
Output: response(dict), containing keys: image_b64
"""
import base64
import io
import logging
from datetime import datetime

import requests
import validators
from PIL import Image, ImageDraw, ImageFont, ImageOps
from PIL.Image import Image as PIL_IMAGE
from pydantic import BaseModel, constr, validator

logging.basicConfig(level=logging.INFO)


class PhishEvent(BaseModel):
    """
    Provides Pydantic validation for event received by Lambda

    Ensures the event received contains the expected keys,
    and values of the expected type and format
    """

    image_link: constr(strip_whitespace=True, min_length=10)
    label_url: constr(strip_whitespace=True, min_length=10)

    @validator("image_link")
    def image_link_valid_link(cls, v):
        if not validators.url(v):
            raise ValueError("image_link must be a valid link")
        return v

    @validator("image_link")
    def image_link_valid_extension(cls, v):
        if not v.endswith((".png", ".jpg", ".jpeg")):
            raise ValueError(
                ("image_link must end with" ".png, .jpg or .jpeg extension")
            )
        return v


def download_image(image_url: str) -> PIL_IMAGE:
    """Downloads image at URL and saves as a Pillow Image object

    Performs a GET request against the image_url,
    storing the file content in a buffer.
    Opens file content with Pillow's Image class,
    and returns the resulting Image object

    :param image_url: the URL of the target image
    :return image: the resulting Pillow Image object
    """
    start_time = datetime.now()

    resp = requests.get(image_url, timeout=1)
    logging.info(
        (
            f"download_image:resp.status_code={resp.status_code},"
            f"type(resp.content)={type(resp.content)}"
        )
    )
    assert resp.ok
    assert isinstance(resp.content, bytes)

    image_bytes = io.BytesIO(resp.content)
    image = Image.open(image_bytes, formats=["PNG", "JPEG"])

    exec_time = (datetime.now() - start_time).total_seconds()
    logging.info(f"download_image:exec_time:{exec_time}")
    return image


def add_border(image: PIL_IMAGE) -> PIL_IMAGE:
    """Add thin black border, so label does not obscure screenshot

    Adds a black border of size 12 around the image. This allows the
    label to be printed onto the border and not over the image itself

    :param image: a Pillow Image object
    :return bordered_image: the same Image with border added
    """
    bordered_image = ImageOps.expand(image, border=12, fill="black")
    return bordered_image


def label_image(image: PIL_IMAGE, label_url: str) -> PIL_IMAGE:
    """Write the text to the top-left corner of the border

    Writes the label_url string over the image,
    starting from the top-left corner in white default font.

    :param image: a Pillow Image object
    :param label_url: the URL from which the image was downloaded
    :return image: the same Image with the label overlayed
    """
    default_font = ImageFont.load_default()
    image_editable = ImageDraw.Draw(image)
    image_editable.text((0, 0), label_url, "white", font=default_font)
    return image


def process_image(raw_image: PIL_IMAGE, label_url: str) -> PIL_IMAGE:
    """Adds border and text label to image

    Calls add_border and label_image functions on the Image received.
    Returns the resulting Image

    :param raw_image: the image as downloaded from the URL,
    with no modifications
    :param label_url: the URL from which the image was downloaded
    :return labeled_image: the image after border and text label are added
    """
    start_time = datetime.now()
    logging.info("entered process_image...")
    bordered_image = add_border(raw_image)
    labeled_image = label_image(bordered_image, label_url)
    exec_time = (datetime.now() - start_time).total_seconds()
    logging.info(f"process_image:exec_time:{exec_time}")
    return labeled_image


def image_to_base64(image: PIL_IMAGE) -> str:
    """Converts Image to a Base64-encoded string

    Exports Image as PNG to a bytes buffer, then encodes
    the contents of the buffer to Base64.

    :param image: a Pillow Image object
    :return base64_string: the Base64 representation of that image
    saved in PNG format
    """
    logging.info("entered image_to_base64...")
    function_start_time = datetime.now()

    bytes_buffer = io.BytesIO()
    logging.info(type(bytes_buffer))

    image.save(bytes_buffer, "PNG", compress_level=3)
    image_bytes = bytes_buffer.getvalue()
    logging.info(f"type(image_bytes)={type(image_bytes)}")

    base64_bytes = base64.b64encode(image_bytes)
    base64_string = base64_bytes.decode("ascii")

    exec_time = (datetime.now() - function_start_time).total_seconds()
    logging.info(f"image_to_base64:exec_time:{exec_time}")
    return base64_string


def handler(event, _context):
    """Equivalent of main() function for Lambda functions in Python

    For more, see:
    https://docs.aws.amazon.com/lambda/latest/dg/python-handler.html

    :param event: a Dictionary representation of an input event
    received by the Lambda runtime
    :return response: the HTTP response data we return to the caller
    """
    start_time = datetime.now()
    assert isinstance(event, dict)
    phish_event = PhishEvent(**event)

    image_link = phish_event.image_link
    label_url = phish_event.label_url

    raw_image = download_image(image_link)
    logging.info("download_image() complete")
    processed_image = process_image(raw_image, label_url)
    logging.info("process_image() complete")
    base64_string = image_to_base64(processed_image)
    logging.info("image_to_base64() complete")

    lambda_response = {
        "image_b64": base64_string
        # "statusCode": 200,
        # "body": json.dumps(
        #    {'image_b64': base64_string
        #     })
    }
    logging.info()
    logging.info(lambda_response)
    exec_time = (datetime.now() - start_time).total_seconds()
    logging.info(f"TOTAL_EXEC-TIME={exec_time}")
    return lambda_response
