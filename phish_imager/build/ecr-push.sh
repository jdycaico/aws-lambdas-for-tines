#!/bin/bash
# takes a param for the env you want to deploy to (prod, personal)
# dont forget to populate aws envvars
export AWS_ACCT_ID=632258967672 #csirt-prod
#export AWS_ACCT_ID=800303436368 personal-aws
#export AWS_ACCT_ID=735870510174 #csirt-sandbox

aws ecr get-login-password --region us-west-2 | docker login --username AWS --password-stdin $AWS_ACCT_ID.dkr.ecr.us-west-2.amazonaws.com
docker tag phish-imager:latest $AWS_ACCT_ID.dkr.ecr.us-west-2.amazonaws.com/phish-imager:latest
docker push $AWS_ACCT_ID.dkr.ecr.us-west-2.amazonaws.com/phish-imager:latest

#cleanup dangling (untagged) images
danglers=`docker images -f "dangling=true" -q`
# if any hashes returned, remove the assoc images
if [ ${#danglers} -gt 0 ]
then
    docker rmi $(docker images -f "dangling=true" -q) --force
fi

aws lambda update-function-code \
--function-name phish-imager \
--image-uri "$AWS_ACCT_ID.dkr.ecr.us-west-2.amazonaws.com/phish-imager:latest"
