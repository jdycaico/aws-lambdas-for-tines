export PROJECT_ROOT=`git rev-parse --show-toplevel`
echo $PROJECT_ROOT
#pipenv shell

export APPDIR=$PROJECT_ROOT/phish_imager
cd $APPDIR
echo $APPDIR

echo "running app.py from $(pwd) (pipenv)"
pipenv run python app.py --system --no-interactive # test that libs are good

# local tests with simulated lambda
sh $PROJECT_ROOT/test/python-lambda-local/phish/test_all.sh

#docker stuff
pipenv requirements > requirements.txt
echo "combining certificates..."
#echo $PWD
sh certs/combine_certs.sh
echo "Exit code: $?"

echo "starting docker build in $(pwd)"
docker build -t phish-imager .

# TODO: test container locally?
#containerId=`docker run --detach -p 9000:8080 phish-imager`
#docker kill $containerId
