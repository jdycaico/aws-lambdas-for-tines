# Tests the lambda function using the python-lambda-local module, against all events in the current directory
PROJECT_ROOT=$(git rev-parse --show-toplevel)
pipenv shell

OUTFILE='test_all.log'
echo '' > $OUTFILE

FILES="$PROJECT_ROOT/test/python-lambda-local/pdfurl/*.json"
for f in $FILES
do
    echo "Testing with $(basename $f)..."
    python-lambda-local $PROJECT_ROOT/pdf_url_extractor/app.py $f >> $OUTFILE  2>&1 #&>> $OUTFILE # | tee $OUTFILE
    echo "Exit code: $?"
done
echo "\nExit code 0 indicates success"
echo "See output in $OUTFILE"
