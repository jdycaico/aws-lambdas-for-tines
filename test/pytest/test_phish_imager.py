import io
from test.pytest.static_values import PANTHER_IMAGE_B64

import numpy
import PIL
import pytest
import unpaddedbase64
from faker import Faker
from PIL import Image

from phish_imager import app

fake = Faker()

TEST_IMAGE = Image.open(io.BytesIO(unpaddedbase64.decode_base64(PANTHER_IMAGE_B64)))
TEST_LABEL_URL = "https://panther.com/"
TEST_IMAGE_URL_JPG = "https://assets.ubuntu.com/v1/" "7f34ade9-website_suru_25.jpg"


def test_download_image():
    """
    GIVEN a valid image URL (PNG or JPEG) as input
    WHEN download_image is called
    THEN a PIL Image object should be returned, of either kind
    """
    image_urls = [
        TEST_IMAGE_URL_JPG,
        (
            "https://archlinux.org/static/logos/"
            "apple-touch-icon-144x144.38cf584757c3.png"
        ),
    ]
    for url in image_urls:
        image = app.download_image(url)
        assert isinstance(
            image, (PIL.PngImagePlugin.PngImageFile, PIL.JpegImagePlugin.JpegImageFile)
        )


def test_download_image_non_image_url():
    """
    GIVEN a URL that does not resolve to an image
    WHEN download_image is called
    THEN a PIL.UnidentifiedImageError should be raised
    """
    non_image_url = "https://en.wikipedia.org/wiki/Alexis_de_Tocqueville"
    with pytest.raises(PIL.UnidentifiedImageError):
        app.download_image(non_image_url)


def test_download_image_unexpected_type():
    """
    GIVEN a valid image URL, but of an image of an unexpected type
    WHEN download_image is called
    THEN a PIL.UnidentifiedImageError should be raised
    """
    gif_image_url = "https://media.tenor.com/MYZgsN2TDJAAAAAC/this-is.gif"
    with pytest.raises(PIL.UnidentifiedImageError):
        app.download_image(gif_image_url)


def test_add_border():
    """
    GIVEN a valid PIL Image object
    WHEN add_border is called
    THEN the resulting Image dimensions should exceed the original
    dimensions by 24 pixels in both width and height
    """
    bordered_image = app.add_border(TEST_IMAGE)
    size_difference = tuple(numpy.subtract(bordered_image.size, TEST_IMAGE.size))
    assert size_difference == (24, 24)


def test_label_image():
    """
    GIVEN a valid PIL Image object as input
    WHEN label_image is called
    THEN the resulting image is different than the input image
    """
    test_b64 = unpaddedbase64.encode_base64(TEST_IMAGE.tobytes())
    labeled_image = app.label_image(TEST_IMAGE, label_url=TEST_LABEL_URL)
    labeled_b64 = unpaddedbase64.encode_base64(labeled_image.tobytes())
    assert test_b64 != labeled_b64


def test_image_to_base64():
    """
    GIVEN a PIL Image object
    WHEN image_to_base64 is called
    THEN a valid base64 string will be returned
    """
    image_b64 = app.image_to_base64(TEST_IMAGE)
    image_bytes = unpaddedbase64.decode_base64(image_b64)
    assert isinstance(image_bytes, bytes)
