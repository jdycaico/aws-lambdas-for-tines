import io
from test.pytest.static_values import CAMEFROM_PDF_B64, CAMUS_PDF_B64

import unpaddedbase64
from faker import Faker

from pdf_url_extractor import app

fake = Faker()


def test_decode_b64_string():
    """
    GIVEN a valid base64 string as input, with no padding characters
    WHEN decode_b64_string is called
    THEN a bytes buffer should be ≈returned containing an equivalent bytestring
    """
    pdf_bytes_buffer = app.decode_b64_string(CAMUS_PDF_B64)
    assert isinstance(pdf_bytes_buffer, io.BytesIO)
    contents = pdf_bytes_buffer.read()
    encoded_contents = unpaddedbase64.encode_base64(contents)
    assert encoded_contents == CAMUS_PDF_B64


def test_preview_event_short_event():
    """
    GIVEN an event with string length shorter than 100 characters
    WHEN preview_event is called
    THEN the event is not truncated but preserved in full
    """
    event = {"pdf_b64": "SGVsbG8gV29ybGQh"}
    resp = app.preview_event(event)
    assert not len(str(resp)) < len(str(event))


def test_preview_event_long_event():
    """
    GIVEN an event with string length exceeding 100 characters
    WHEN preview_event is called
    THEN the event is truncated accordingly
    """
    event = {"pdf_b64": fake.pystr(min_chars=100, max_chars=200)}
    resp = app.preview_event(event)
    assert len(str(resp)) < len(str(event))
    assert len(str(resp)) == 86


def test_extract_urls_from_annotations_annotations_exist():
    """
    GIVEN a bytes-buffer representation of a PDF
    containing URLs in its Annotations
    WHEN extract_urls_from_annotations is called
    THEN a list of the contained URLs will be returned
    """
    pdf_bytes_buffer = app.decode_b64_string(CAMUS_PDF_B64)
    link_urls = app.extract_urls_from_annotations(pdf_bytes_buffer)
    assert isinstance(link_urls, list)
    assert len(link_urls) > 0


def test_extract_urls_from_annotations_annotations_noexist():
    """
    GIVEN a bytes-buffer representation of a PDF
    with no URLs in its Annotations
    WHEN extract_urls_from_annotations is called
    THEN an empty list will be returned
    """
    pdf_bytes_buffer = app.decode_b64_string(CAMEFROM_PDF_B64)
    link_urls = app.extract_urls_from_annotations(pdf_bytes_buffer)
    assert isinstance(link_urls, list)
    assert len(link_urls) == 0


def test_extract_urls_from_text_urls_present():
    """
    GIVEN a bytes-buffer representation of a PDF containing URLs in its text
    WHEN extract_urls_from_text is called
    THEN a list of the contained URLs will be returned
    """
    pdf_bytes_buffer = app.decode_b64_string(CAMUS_PDF_B64)
    text_urls = app.extract_urls_from_text(pdf_bytes_buffer)
    assert isinstance(text_urls, list)
    assert len(text_urls) > 0


def test_extract_urls_from_text_nourls():
    """
    GIVEN a bytes-buffer representation of a PDF with no URLs in its text
    WHEN extract_urls_from_text is called
    THEN an empty list will be returned
    """
    pdf_bytes_buffer = app.decode_b64_string(CAMEFROM_PDF_B64)
    text_urls = app.extract_urls_from_text(pdf_bytes_buffer)
    assert isinstance(text_urls, list)
    assert len(text_urls) == 0


def test_get_final_urls_list_basecase():
    """
    GIVEN two lists of valid URLs with no overlap
    WHEN get_final_urls_list is called
    THEN the sum of those lists will be returned
    """
    first_list = [
        "https://security.stackexchange.com/",
        "https://news.ycombinator.com/",
    ]

    second_list = ["https://hackernoon.com/c/cybersecurity", "https://threatpost.com/"]

    final_urls = app.get_final_urls_list(first_list, second_list)
    summed_lists = first_list + second_list
    assert sorted(final_urls) == sorted(summed_lists)


def test_get_final_urls_list_invalid_urls():
    """
    GIVEN two lists of URLs, the first containing invalid urls
    and the second containing valid URLs
    WHEN extract_urls_from_text is called
    THEN a list equal to the second input list will be returned
    """

    first_list = ["x//ifeoi.ssssss", "kqr.5c89"]

    second_list = ["https://hackernoon.com/c/cybersecurity", "https://threatpost.com/"]

    final_urls = app.get_final_urls_list(first_list, second_list)

    assert sorted(final_urls) == sorted(second_list)


def test_get_final_urls_list_deduplication():
    """
    GIVEN two lists of valid URLs that include 2 identical items
    WHEN extract_urls_from_text is called
    THEN a list of URLs will be returned that excludes the duplicate items
    """
    first_list = [
        "https://security.stackexchange.com/",
        "https://news.ycombinator.com/",
        "https://danielmiessler.com/",
        "https://www.blackhillsinfosec.com/",
    ]

    second_list = [
        "https://hackernoon.com/c/cybersecurity",
        "https://threatpost.com/",
        "https://danielmiessler.com/",
        "https://www.blackhillsinfosec.com/",
    ]

    final_urls = app.get_final_urls_list(first_list, second_list)
    expected_urls = list(set(first_list + second_list))

    assert sorted(final_urls) == sorted(expected_urls)
