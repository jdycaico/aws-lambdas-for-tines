# holds static values shared across multiple tests

from pathlib import Path

import git

PROJECT_ROOT = Path(git.Repo().git.rev_parse("--show-toplevel"))
BASE64_DIR = PROJECT_ROOT / "test" / "pytest" / "base64"

ENDPOINT_URL = "http://localhost:9000/2015-03-31/functions/" "function/invocations"
HEADERS = {"Content-Type": "application/json"}

CAMUS_PDF_B64 = (BASE64_DIR / "camus_pdf.b64").read_text()

CAMEFROM_PDF_B64 = (BASE64_DIR / "camefrom_pdf.b64").read_text()

PANTHER_IMAGE_B64 = (BASE64_DIR / "panther_image.b64").read_text()
