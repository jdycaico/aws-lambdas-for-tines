"""Pdf-Url-Extractor
AWS Lambda function to process PDF files and
return all the URLs contained therein.

Process:
> Reconstructs a PDF file from a base64-string representation
> Extracts URLs from the file
> Returns a list of URLs extracted, along with the count of URLs extracted

Input: event(dict), containing keys: pdf_b64
Output: response(dict), containing keys: urls, url_count
"""
import io
import logging
import re
from datetime import datetime
from pathlib import Path
from typing import List

import fitz  # aka PyMuPDF
import git
import unpaddedbase64
import validators
from pydantic import BaseModel, constr, validator

logging.basicConfig(level=logging.ERROR)
PROJECT_ROOT = Path(git.Repo().git.rev_parse("--show-toplevel"))


class PdfEvent(BaseModel):
    """
    Provides Pydantic validation for event received by Lambda

    Ensures the event received is of a valid format, and contains
    keys of the right types and with valid values
    """

    pdf_b64: constr(strip_whitespace=True, min_length=300)

    @validator("pdf_b64")
    def pdf_b64_valid_base64(cls, v):
        try:
            unpaddedbase64.decode_base64(v)
        except Exception as e:
            raise ValueError(f"pdf_b64 must be valid Base64\n{e}")
        return v


def read_regex_file() -> str:
    """
    Reads the URL extraction regex from local file

    Source: github.com/metachris/pdfx/blob/master/pdfx/extractor.py
    """
    regex_path = PROJECT_ROOT / "pdf_url_extractor" / "regex"
    regex_string = regex_path.read_text()
    return regex_string


def decode_b64_string(b64_string: str) -> io.BytesIO:
    """
    Decodes base64 string into bytes, stores in buffer

    Converts a base64-encoded string of a PDF file into a bytes buffer
    of the PDF file data

    :param b64_string: string of base64-encoded PDF file
    :return pdf_bytes_bugger: in-memory buffer of PDF file data
    """
    logging.info("starting decode_b64_string...")
    start_time = datetime.now()
    pdf_bytes = unpaddedbase64.decode_base64(b64_string)
    assert isinstance(pdf_bytes, bytes)
    logging.info(
        (
            f"decode_b64_string.exectime:"
            f"{(datetime.now() - start_time).total_seconds()}"
        )
    )
    pdf_bytes_buffer = io.BytesIO(pdf_bytes)
    return pdf_bytes_buffer


def preview_event(event: dict) -> dict:
    """Truncates the event if longer than 100 characters

    Events received by the lambda can contain long base64 strings
    which clog up the terminal output. This function mitigates this
    by trimming the string representation of long events.

    :param event: the event dictionary, as received
    by the Lambda handler function
    :return event_preview: a single-key dictionary, with the
    'event' key's value being a String preview of the input event

    """
    if len(str(event)) > 100:
        event_preview = {"event": f"{str(event)[:40]}...{str(event)[-30:]}"}
    else:
        event_preview = {"event": str(event)}
    return event_preview


def extract_urls_from_annotations(pdf_bytes_buffer: io.BytesIO) -> List[str]:
    """Parses PDF annotations to extract any URLs present

    The PDF standard includes a section for a type of content called
    Annotations. Sometimes URLs can be stored in this area of the PDF,
    particularly when dealing with hyperlinks. For more, see
    https://pspdfkit.com/blog/2018/what-are-annotations/.
    This function used to use PyPDF, in which PDF links are stored in
    /Annotations, but has been refactored to use PyMuPDF, in which
    they are accessed in Page.links

    :param pdf_bytes_buffer: a PDF file's contents stored in
    an in-memory buffer
    :return link_urls: a list of URL strings captured
    from the PDF's annotations
    """
    start_time = datetime.now()
    link_urls = []
    fitz_reader = fitz.Document(stream=pdf_bytes_buffer)
    for page in fitz_reader:
        for link in page.links():
            link_urls.append(link["uri"])
    logging.info(f"link_urls:\n{link_urls}")
    exectime = (datetime.now() - start_time).total_seconds()
    logging.info(f"extract_urls_from_annotations.exectime={exectime}")
    return link_urls


def extract_urls_from_text(pdf_bytes_buffer: io.BytesIO) -> List[str]:
    """Extracts URLs from the raw text of a PDF using a Regex

    Reads the regular expression from a local file. Loops through each
    page of the PDF file, extracting all URLs using the regex.
    Returns a List containing the extracted URLs as strings

    :param pdf_bytes_buffer: a PDF file's contents
    stored in an in-memory buffer
    :return text_urls: a list of URL strings
    captured from the PDF's text content
    """
    logging.info("extracting URLs from text...")
    url_regex = read_regex_file()
    start_time = datetime.now()
    rawtext = ""
    time_before_loop = datetime.now()
    fitz_reader = fitz.Document(stream=pdf_bytes_buffer)
    for page in fitz_reader:
        rawtext += page.get_text()
    loop_exectime = (datetime.now() - time_before_loop).total_seconds()
    logging.info(f"time for page-loop w/ extract-text={loop_exectime}")
    text_urls = re.findall(url_regex, rawtext, re.IGNORECASE)
    logging.info(f"text_urls:\n{text_urls}")
    exectime = (datetime.now() - start_time).total_seconds()
    logging.info(f"extract_urls_from_text.exectime={exectime}")
    return text_urls


def get_final_urls_list(urls_1: List[str], urls_2: List[str]) -> List[str]:
    """Combines, deduplicates, and validates 2 input lists of URLs

    First, we sum the lists. Then, we trim any duplicates present.
    Finally, the validators library is used to ensure
    each item in the combined list is a valid URL

    :param urls_1: a List of URL strings
    :param urls_2: a second List of URL strings
    :return final_urls: processed output of the function as described above
    """
    all_urls = urls_1 + urls_2
    distinct_urls = set(all_urls)
    final_urls = [x for x in distinct_urls if validators.url(x)]
    return final_urls


def handler(event, _context):
    """Equivalent of main() function for Lambda functions in Python

    For more, see:
    https://docs.aws.amazon.com/lambda/latest/dg/python-handler.html

    :param event: a Dictionary representation of an input event
    received by the Lambda runtime
    :return response: the HTTP response data we return to the caller
    """
    start_time = datetime.now()
    assert isinstance(event, dict)
    logging.info(preview_event(event))

    pdf_event = PdfEvent(**event)

    pdf_b64 = pdf_event.pdf_b64
    pdf_bytes_buffer = decode_b64_string(pdf_b64)
    logging.info("COMPLETED B64 DECODE")

    urls_from_annotations = extract_urls_from_annotations(pdf_bytes_buffer)
    logging.info("COMPLETED URL EXTRACTION FROM PDF-ANNOTATIONS")

    urls_from_text = extract_urls_from_text(pdf_bytes_buffer)
    logging.info("COMPLETED URL EXTRACTION FROM RAW-TEXT")

    final_urls = get_final_urls_list(urls_from_text, urls_from_annotations)

    logging.info()
    logging.info(f"final urls: {final_urls}")
    assert isinstance(final_urls, list)

    response = {
        "urls": final_urls,
        "url_count": len(final_urls)
        # "statusCode": 200,
        # "body": json.dumps({
        #    'urls': final_urls,
        #    'url_count': len(final_urls)
        # })
    }
    logging.info(response)
    exec_time = (datetime.now() - start_time).total_seconds()
    logging.info(f"TOTAL_EXEC-TIME={exec_time}")
    return response
