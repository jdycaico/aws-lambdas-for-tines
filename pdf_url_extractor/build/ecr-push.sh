#populate aws envvars, then proceed
#800303436368 personal-aws
#735870510174 #csirt-sandbox

#export AWS_ACCT_ID=800303436368 # personal-aws
export AWS_ACCT_ID=632258967672 #csirt-prod

aws ecr get-login-password --region us-west-2 | docker login --username AWS --password-stdin $AWS_ACCT_ID.dkr.ecr.us-west-2.amazonaws.com
docker tag pdf-url-extractor:latest $AWS_ACCT_ID.dkr.ecr.us-west-2.amazonaws.com/pdf-url-extractor:latest
docker push $AWS_ACCT_ID.dkr.ecr.us-west-2.amazonaws.com/pdf-url-extractor:latest


#cleanup dangling (untagged) images
danglers=`docker images -f "dangling=true" -q`
# if any hashes returned, remove the assoc images
if [ ${#danglers} -gt 0 ] 
then
    docker rmi $(docker images -f "dangling=true" -q) --force
fi

aws lambda update-function-code \
--function-name pdf-url-extractor \
--image-uri "$AWS_ACCT_ID.dkr.ecr.us-west-2.amazonaws.com/pdf-url-extractor:latest"
#./update-lambda.sh