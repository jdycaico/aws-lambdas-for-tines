export PROJECT_ROOT=`git rev-parse --show-toplevel`
echo $PROJECT_ROOT
#pipenv shell

export APPDIR=$PROJECT_ROOT/pdf_url_extractor
cd $APPDIR
echo $APPDIR

echo "running app.py from $(pwd) (pipenv)"
pipenv run python app.py --system --no-interactive # test that libs are good

# local tests with simulated lambda
sh $PROJECT_ROOT/test/python-lambda-local/pdfurl/test_all.sh

#docker stuff
pipenv requirements > requirements.txt

echo "begin build of docker container"
docker build -t pdf-url-extractor .

# TODO: test container locally?
#containerId=`docker run --detach -p 9000:8080 pdf-url-extractor`
#docker kill $containerId